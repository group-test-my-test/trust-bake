<?php

namespace App\Exports;

use App\Models\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class UsersExport implements FromCollection, WithMapping, WithHeadings
{
    public function map($user): array
    {
        return [
            'id'.$user->vk_id,
            $user->last_name,
            $user->first_name,
            'https://vk.com/wall' . $user->vk_id . '_' . $user->post_id
        ];
    }

    public function headings(): array
    {
        return [
            'vk_id',
            'Фамилия',
            'Имя',
            'Ссылка на пост'
        ];
    }

    /**
    * @return BinaryFileResponse
    */
    public function collection()
    {
        return User::where('competition', true)->get();
//        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
