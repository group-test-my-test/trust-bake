<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;

class BotController extends Controller
{
    public function message()
    {
        $request = json_decode(file_get_contents('php://input'));
        if (!$request) {
            return 'nope';
        }

        if ($request->secret != config('bot.vk_secret_token') && $request->type != 'confirmation') {
            return "nope";
        }

        switch ($request->type) {
            case 'confirmation':
                return config('bot.vk_confirmation_code');
                break;
            case 'app_payload':
                $user_id = $request->object->user_id;
                $payload = json_decode($request->object->payload);

                $user = User::where('vk_id', $user_id)->first();
                if ($user->competition != true) {
                    $user->competition = true;
                    $user->post_id = $payload->post_id;
                    $user->save();

                    $request_params = [
                        'message' => "$user->first_name, благодарим за участие в конкурсе #генератордоверия!
                    \nЧтобы побороться за смартфон, сохрани пост на стене до 24.09 и оставь свой комментарий в группе конкурса под закрепленным постом: https://vk.com/wall-75573663_801
                    \nПобедителя объявим 24 сентября!",
                        'peer_id' => $user_id,
                        'access_token' => config('bot.vk_token'),
                        'v' => '5.103',
                        'random_id' => '0'
                    ];

                    $get_params = http_build_query($request_params);
                    $dump = file_get_contents('https://api.vk.com/method/messages.send?' . $get_params);
                    dump($dump);
                }
                break;
            default:
                return 'ok';
        }
    }
}
