<?php

namespace App\Http\Controllers;

use App\Http\Resources\ReasonResource;
use App\Models\Reason;
use Illuminate\Http\Request;

class ReasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return ReasonResource::collection(Reason::all());
    }
}
