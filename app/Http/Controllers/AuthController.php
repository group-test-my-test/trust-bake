<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function login(UserRequest $request)
    {
        $user = User::updateOrCreate(
            $request->only(['vk_id']),
            $request->validated()
        );

        $token = $user->createToken(Str::random(), [
            'acting-as-user'
        ]);

        return response()->json([
            'message' => 'Вы вошли',
            'access_token' => $token->plainTextToken
        ]);
    }
}
