<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'vk_id' => $this->vk_id,
            'last_name' =>$this->last_name,
            'first_name' =>$this->first_name,
            'competition' => $this->competition,
            'post_id' => $this->post_id,
            ];
    }
}
