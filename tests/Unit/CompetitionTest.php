<?php

namespace Tests\Unit;


use App\Models\User;
use Tests\TestCase;

class CompetitionTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        factory(User::class, 10)->create();
    }

    public function testUsersOutput()
    {
        $response = $this->getJson(route('competition'));
        $response->dump();
    }
}
