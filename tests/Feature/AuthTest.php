<?php

namespace Tests\Feature;


use App\Models\User;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /** @var array */
    private $userData;

    protected function setUp(): void
    {
        parent::setUp();
        factory(User::class, 10)->create();
        $this->userData = [
            'vk_id' => '1234567',
            'last_name' => 'Bonjure',
            'first_name' => 'Gazz'
        ];
    }

    /**
     * Пользователь заходит в приложение впервые
     * - проверка, что пользователь несуществует
     * - создать пользователя с параметрами:
     *   - имя
     *   - фамилия
     *   - vk_id
     * - дать access_token пользователю
     */
    public function testLoginNewUser()
    {
        $this->assertNull(
            User::where('vk_id', $this->userData['vk_id'])->first()
        );

        $response = $this->postJson(route('auth.login'), $this->userData);

        $response->dump()->assertJsonStructure([
            'message',
            'access_token'
        ]);

        $user = User::where('vk_id', $this->userData['vk_id'])->first();
        $token = $user->tokens->first();

        $this->assertNotNull(
            $user
        );

        $this->assertNotNull($token);
        $this->assertTrue(in_array('acting-as-user', $token->abilities));
    }
}
