<?php

namespace Tests\Feature;


use Tests\TestCase;

class VkBotTest extends TestCase
{
    public function testConfirmation()
    {
        $data = [
        'type' => 'app_payload',
        'object' => [
            'user_id'=> 86190353,
            'app_id' => 7568788,
            'payload' => [
                'id' => 86190353,
                'post_id' => 1242
                ]
            ],
        'group_id' => 198019733,
        'event_id' => 'df784675693f8790d865676d6ba0f72e564e34c8',
        'secret' => 'MaxPuska'
         ];

        $response = $this->postJson(route('bot'), $data);
        dump($response);
    }

    public function testMessage()
    {
        $data = [
            'type' => 'message_new',
            'object' => [
                'id' => 2,
                'date' => 1597936630,
                'out'=> 0,
                'user_id' => 86190353,
                'read_state'=> 0,
                'title' => null,
                'body' => 'привет',
                'owner_ids' => [
                 ]
             ],
            'group_id' => 198019733,
            'event_id'=> '812e5791e25d2e1f0497a16d4139b0cc15b15b6a',
            'secret' => 'MaxPuska'
        ];


        $response = $this->postJson(route('bot'), $data);

        dump($response);
    }
}
