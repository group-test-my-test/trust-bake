<?php

use App\Models\Reason;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPersistentReason extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $reasons = json_decode(
            file_get_contents(database_path('presets/ReasonPreset.json')),
            true
        );

        foreach ($reasons as $reason) {
            factory(Reason::class)->create($reason);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reasons');
    }
}
