<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Reason;
use Faker\Generator as Faker;

$factory->define(Reason::class, function (Faker $faker) {
    return [
        'text' => $faker->words(random_int(3,6), true),
    ];
});
