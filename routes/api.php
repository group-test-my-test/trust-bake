<?php

use App\Exports\UsersExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'before' => 'auth',
    'prefix' => 'auth',
    'as' => 'auth.'
], function () {
    Route::post('login', 'AuthController@login');
});

Route::get('users/download', function () {
    return Excel::download(new UsersExport(), 'users.xlsx');
});

Route::get('subjects', 'SubjectController@index');
Route::get('users', 'UserController@index');
Route::get('reasons', 'ReasonController@index');

Route::post('bot', 'BotController@message')->name('bot');
